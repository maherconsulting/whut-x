﻿using System;
using System.Windows.Forms;
using Hotkeys;
using System.Drawing;
using System.IO;

namespace WHUT_X
{
    public partial class Form1 : Form
    {
        public string out_dir = string.Empty;
        private Hotkey hk_parse;
        private bool shown_help = false;

        private static NotifyIcon n;

        public static NotifyIcon Notifier
        {
            get { return n; }
        }

        public Form1()
        {
            InitializeComponent();
            n = notifier;

            hk_parse = new Hotkey(Keys.P, false, false, true, true);
            hk_parse.Pressed += delegate { execute_parse(); };

            if (hk_parse.GetCanRegister(this))
                hk_parse.Register(this);
            else
                notifier.ShowBalloonTip(1, "WHUT-X Error", "Unable to register Hotkey WIN + ALT + P", ToolTipIcon.Error);

            FolderBrowserDialog output = new FolderBrowserDialog();
            output.Description = "Set WHUT-X output directory";
            output.ShowNewFolderButton = true;

            if (output.ShowDialog() == DialogResult.OK)
            {
                out_dir = output.SelectedPath;
            }
            else
            {
                Application.Exit();
            }
        }

        public void execute_parse()
        {
            string txt = Clipboard.GetText();

            if (character.parse_talent_string(txt))
            {
                int screenHeight = Screen.PrimaryScreen.WorkingArea.Height;
                int screenWidth = Screen.PrimaryScreen.WorkingArea.Width;

                Form2 f2 = new Form2(txt);
                f2.StartPosition = FormStartPosition.Manual;
                f2.TopMost = true;
                f2.Location = new Point(screenWidth - f2.Width, screenHeight - f2.Height);

                bool write_result = false;

                if (f2.ShowDialog() == DialogResult.OK)
                {
                    character.custom_note = f2.txt_note;
                    string file = character.generate_filename();
                    if (File.Exists(@out_dir + "\\" + file))
                    {
                        if (MessageBox.Show(file + " already exists, do you wish to overwrite?", "File Overwrite Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            write_result = character.write_xml(out_dir);
                        }
                    }
                    else
                    {
                        write_result = character.write_xml(out_dir);
                    }

                    if (!write_result && !string.IsNullOrEmpty(character.error_message))
                    {
                        notifier.ShowBalloonTip(1, "WHUT-X Error", character.error_message, ToolTipIcon.Error);
                    }
                    
                }
            }
            else
            {
                notifier.ShowBalloonTip(1, "WHUT-X Error", character.error_message, ToolTipIcon.Error);
            }
        }

        private void menu_exit_Click(object sender, EventArgs e)
        {
            notifier.Visible = false;
            Application.Exit();
        }

        private void notifier_MouseMove(object sender, MouseEventArgs e)
        {
            if (!shown_help)
            {
                shown_help = true;
                notifier.ShowBalloonTip(1, "WHUT-X", "Copy Wowhead talent URL to clipboard then use hotkey WIN + ALT + P to convert the URL to XML", ToolTipIcon.None);
            }
        }

    }
}
