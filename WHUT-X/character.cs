﻿using System;
using System.IO;
using System.Reflection;

namespace WHUT_X
{
    public class character
    {
        private static decode_t[] decoding = new decode_t[] {

            new decode_t('0', '0', '0'),new decode_t('z', '0', '1'),new decode_t('M', '0', '2'),new decode_t('c', '0', '3'),new decode_t('m', '0', '4'),new decode_t('V', '0', '5'),
            new decode_t('o', '1', '0'),new decode_t('k', '1', '1'),new decode_t('R', '1', '2'),new decode_t('s', '1', '3'),new decode_t('a', '1', '4'),new decode_t('q', '1', '5'),
            new decode_t('b', '2', '0'),new decode_t('d', '2', '1'),new decode_t('r', '2', '2'),new decode_t('f', '2', '3'),new decode_t('w', '2', '4'),new decode_t('i', '2', '5'),
            new decode_t('h', '3', '0'),new decode_t('u', '3', '1'),new decode_t('G', '3', '2'),new decode_t('I', '3', '3'),new decode_t('N', '3', '4'),new decode_t('A', '3', '5'),
            new decode_t('L', '4', '0'),new decode_t('p', '4', '1'),new decode_t('T', '4', '2'),new decode_t('j', '4', '3'),new decode_t('n', '4', '4'),new decode_t('y', '4', '5'),
            new decode_t('x', '5', '0'),new decode_t('t', '5', '1'),new decode_t('g', '5', '2'),new decode_t('e', '5', '3'),new decode_t('v', '5', '4'),new decode_t('E', '5', '5'),
            new decode_t('\0','\0','\0')

        };
        
        private static string       class_name = "";
        private static int[]        limits;
        private static int          speciality;
        private static string       url_code;
        private static int[]        spec_points;
        private static int[][]      talent_trees;
        private static string[][]   spells; 
        private static string[]     specs;
        private static string       filename = "";
        private static string       error = "";
        private static string       note = "";
        private static string[]     specs_long;

        public static string error_message
        {
            get { return error; }
        }

        public static string custom_note
        {
            set { note = value; }
        }

        public static string output_filename
        {
            get { return filename; }
        }

        private static string clean_talent_string(string t) 
        {
            if (t.Contains("talent#"))
                t = t.Split('#')[1];

            if (t.IndexOf(':') > 0)
                t = t.Substring(0, t.IndexOf(':'));

            return t;
        }

        public static bool parse_talent_string(string talent_string)
        {
            error           = string.Empty;

            if (talent_string.Length == 0)
            {
                error = "Empty talent string";
                return false;
            }

            url_code        = talent_string;
            talent_trees    = new int[3][];
            spells          = new string[3][];
            spec_points     = new int[] { 0, 0, 0 };
            speciality      = 0;
            specs           = new string[] { "", "", "" };
            specs_long      = new string[] { "", "", "" };

            talent_string = clean_talent_string(talent_string);

            get_character(talent_string[0]);

            if (limits == null)
            {
                return false;
            }

            for (int i = 0; i < talent_trees.Length; i++)
            {
                talent_trees[i] = new int[limits[i]];
                spells[i] = new string[limits[i]];
            }


            if (!get_spellnames())
                return false;

            int tree = 0;
            int count = 0;
            int ccount = 0;

            bool spec_defined = false;

            for (int i = 1; i < talent_string.Length; i++)
            {
                char c = talent_string[i];
                ccount++;
                
                bool max_talent = false;
                
                max_talent = Math.Round((decimal)talent_trees[tree].Length / 2) == (ccount - 1);
                if (c == 'Z' || max_talent)
                {
                    count = 0;
                    tree++;

                    if (c == 'Z')
                    {
                        if (!max_talent)
                            speciality++;
                        else
                            spec_defined = true;

                        ccount = 0;
                        continue;
                    }
                }

                if (tree >= 3)
                    break;

                decode_t decode = null;

                for (int j = 0; decoding[j].key != '\0' && decode == null; j++)
                {

                    if (decoding[j].key == c)
                        decode = decoding[j];
                }

                if (decode == null)
                {
                    error = String.Format("Malformed wowhead talent string. Translation for '{0}' unknown.\n", c);
                    return false;
                }


                talent_trees[tree][count++] += ToInt(decode.first);

                if(count != talent_trees[tree].Length)
                    talent_trees[tree][count++] += ToInt(decode.second);

                spec_points[tree] += ToInt(decode.first) + ToInt(decode.second);
            }

            if ((ccount < Math.Round((decimal)talent_trees[tree].Length / 2)) && (speciality>0) && (!spec_defined))
                speciality--;

            return true;
        }

        public static string generate_filename()
        {
            filename  = class_name + "_" + specs[speciality];
            filename += (note.Length > 0) ? "_" + note.Replace(" ","") : "";
            filename += ".xml";

            return filename;
        }

        public static bool write_xml(string path)
        {
            if (talent_trees.Length > 0 && filename.Length > 0)
            {
                try
                {
                    using (StreamWriter write = new StreamWriter(@path + "\\" + filename))
                    {
                        write.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                        write.WriteLine("<!-- Generated from WHUT-X by 74113083 -->");
                        write.WriteLine("<!-- http://www.wowhead.com/talent#" + url_code + " -->");
                        write.WriteLine(String.Format("<TalentTree Specialization=\"{0}\" Name=\"[{1}] {2} [{3}]\" Class=\"{4}\">", (speciality + 1), class_name, specs[speciality], note, class_name.Replace(" ","")));

                        int[] order = null;

                        switch (speciality)
                        {
                            case 0: order = new int[] { 0, 1, 2 }; break;
                            case 1: order = new int[] { 1, 0, 2 }; break;
                            case 2: order = new int[] { 2, 1, 0 }; break;
                        }

                        for (int i = 0; i < order.Length; i++)
                        {
                            write.WriteLine(String.Format("  <!-- {0} Talents -->", specs_long[order[i]]));
                            for (int j = 0; j < talent_trees[order[i]].Length; j++)
                            {
                                if (talent_trees[order[i]][j] != 0)
                                {
                                    write.WriteLine(String.Format("  <Talent Tab=\"{0}\" Index=\"{1}\" Count=\"{2}\" Name=\"{3}\" />", (order[i] + 1), (j + 1), talent_trees[order[i]][j], spells[order[i]][j]));
                                }
                            }
                        }
                        write.WriteLine("</TalentTree>");
                        write.Close();

                        return true;
                    }
                }
                catch (Exception e)
                {
                    error = e.Message;
                    return false;
                }
            }
            else
            {
                error = "No trees to output or filename not set";
                return false;
            }
        }

        private static bool get_spellnames()
        {
            if (string.IsNullOrEmpty(class_name))
                return false;

            try
            {
                string p = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                using (StreamReader s = new StreamReader(@p + "\\spells.dat"))
                {
                    string line = string.Empty;
                    bool found = false;
                    int line_num = 1;
                    while (!s.EndOfStream)
                    {
                        line = s.ReadLine();

                        line_num++;

                        if (line == class_name)
                        {
                            found = true;

                            for (int i = 0; i < spells.Length; i++)
                            {
                                line = s.ReadLine();
                                line_num++;

                                string[] spellnames = line.Split('~');

                                specs_long[i] = spellnames[0];

                                if (spells[i].Length != spellnames.Length - 1)
                                {
                                    error = "Malformed spells.dat file. Failed on line #" + (line_num-1).ToString();
                                    return false;
                                }

                                for (int j = 1; j <= spells[i].Length; j++)
                                {
                                    spells[i][j - 1] = spellnames[j];
                                }
                            }
                        }
                        else
                        {
                            s.ReadLine();
                            s.ReadLine();
                            s.ReadLine();

                            line_num += 3;
                        }
                    }

                    if(!found) {
                        error = "Malformed spells.dat file. Unable to match class.";
                        return false;
                    }

                    s.Close();
                }
            }
            catch
            {
                error = "Unable to load spells.dat file.";
                return false;
            }

            return true;
        }

        private static void get_character(char char_key)
        {
            switch (char_key)
            {
                case 'j':

                    class_name = "Death Knight";
                    
                    limits  = new int[] { 20, 20, 20 };

                    specs   = new string[] { 
                        "Blood", "Frost", "Unholy" 
                    };

                    break;

                case '0':
                    class_name = "Druid";
                    limits = new int[] { 20, 22, 21 };
                    specs = new string[] { "Balance", "Feral", "Resto" };
                    break;

                 case 'c':
                    class_name = "Hunter";
                    limits = new int[] { 19, 19, 20 };
                    specs = new string[] { "BM", "MM", "Surv" };
                    break;

                case 'o':
                    class_name = "Mage";
                    limits = new int[] { 21, 21, 19 };
                    specs = new string[] { "Arcane", "Fire", "Frost" };
                    break;

                case 's':
                    class_name = "Paladin";
                    limits = new int[] { 20, 20, 20 };
                    specs = new string[] { "Holy", "Prot", "Ret" };
                    break;

                case 'b':
                    class_name = "Priest";
                    limits = new int[] { 21, 21, 21 };
                    specs = new string[] { "Disc", "Holy", "Shadow" };
                    break;

                case 'f':
                    class_name = "Rogue";
                    limits = new int[] { 19, 19, 19 };
                    specs = new string[] { "Assassination", "Combat", "Sub" };
                    break;

                case 'h':
                    //Shaman,Ele,Enhance,Resto
                    class_name = "Shaman";
                    limits = new int[] { 19, 19, 20 };
                    specs = new string[] { "Ele", "Enhance", "Resto" };
                    break;

                case 'I':
                    class_name = "Warlock";
                    limits = new int[] { 18, 19, 19 };
                    specs = new string[] { "Afflic", "Demo", "Destro" };
                    break;

                case 'L':
                    class_name = "Warrior";
                    limits = new int[] { 20, 21, 20 };
                    specs = new string[] { "Arms", "Fury", "Prot" };
                    break;

                default:
                    class_name = "";
                    limits = null;
                    specs = null;
                    error = "Unable to determine character class";
                    break;
            }
        }

        private static int ToInt(char c)
        {
            return (int)(c - '0');
        }

    }

}
