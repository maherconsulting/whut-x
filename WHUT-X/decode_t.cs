﻿namespace WHUT_X
{
    public class decode_t
    {
        public decode_t(char k, char f, char s)
        {
            key = k;
            first = f;
            second = s;
        }

        public char key;
        public char first;
        public char second;
    }
}
