﻿using System.Drawing;
using System;
using System.Runtime.InteropServices;
namespace WHUT_X
{
public sealed class WindowsTaskbar
{
private const string ClassName = "Shell_TrayWnd";

public static Rectangle TaskbarPostion
{
get
{
IntPtr taskbarHandle = User32.FindWindow(WindowsTaskbar.ClassName, null);

RECT r = new RECT();
User32.GetWindowRect(taskbarHandle, ref r);

return Rectangle.FromLTRB(r.left, r.top, r.right, r.bottom);
}
}
}

public static class User32
{
[DllImport("user32.dll", SetLastError = true)]
public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

[DllImport("user32.dll")]
[return: MarshalAs(UnmanagedType.Bool)]
public static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);
}

[StructLayout(LayoutKind.Sequential)]
public struct RECT
{
public int left;
public int top;
public int right;
public int bottom;
}
}
