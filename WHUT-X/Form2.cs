﻿using System;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace WHUT_X
{
    public partial class Form2 : Form
    {
        public string txt_note
        {
            get
            {
                return note.Text;
            }
        }

        public Form2(string url)
        {
            InitializeComponent();

            talent_url.Text = url;
            note.Focus();
        }

        private void menu_ok_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(note.Text, "^[A-Za-z0-9\x20]+$"))
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                Form1.Notifier.ShowBalloonTip(1, "Unacceptable Input", "Only A-Za-z0-9 accepted", ToolTipIcon.Error);
            }
        }

        private void menu_cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void Form2_Shown(object sender, EventArgs e)
        {
            this.Activate();
        }
    }
}
